cost 500
[root@ospf2 vagrant]# tracepath 10.10.30.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.201.1                                         1.031ms
 1:  192.168.201.1                                         1.131ms
 2:  10.10.30.1                                            6.295ms reached
     Resume: pmtu 1500 hops 2 back 2


cost 5
[root@ospf2 vagrant]# tracepath 10.10.30.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  10.10.30.1                                            1.123ms reached
 1:  10.10.30.1                                            1.843ms reached
     Resume: pmtu 1500 hops 1 back 1
